=== Raketech review plugin ===
Contributors: Attila Albert
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html


== Description ==
Raketech review plugin to organize reviews

== Installation ==
1. change in gulpfile.js the 'applicationPath' to your respective wordpress installation directory
2. build with 'gulp build' bash command


== Frequently Asked Questions ==


== Changelog ==

= 1.0 =
- Initial Version
